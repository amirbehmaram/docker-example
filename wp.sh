#!/bin/bash
docker-compose -v >/dev/null 2>&1 || { echo >&2 "docker-compose is required"; exit 1; }
docker-compose exec php wp-cli.phar --path=web --allow-root "$@"
