<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'project');

/** MySQL database username */
define('DB_USER', 'project');

/** MySQL database password */
define('DB_PASSWORD', 'project');

/** MySQL hostname */
define('DB_HOST', 'mysql');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2NFsu&a@9o9!L0Y3Qf#*NzQzmwUCA+_d=a]`yk&J?w%+OTC;W%,<3OEa^oFF]_9g');
define('SECURE_AUTH_KEY',  '{AzD6nQoDxPSIlX4|uPnpRUEQOTE[wv}Y]P^g:L0YzWr:N@xPj+Wy_,/1}&nm7us');
define('LOGGED_IN_KEY',    '_t0D5B@,eLLk7mO:TuJJNhf&hcBEetFX5stx1WF4u*Vq0{-}d:{z7$Lwz1&Of`g,');
define('NONCE_KEY',        '2D`OoG0DU:1~, ?04XhfOmF[mDL}*-rDb12Xbr)|/5[/GVD0tS??KMSrEx$7z!j1');
define('AUTH_SALT',        'jf|Xk(_*ekLl5)e5TOMy3w|Tzro8<eA99+7Y1|+e s>-i@?;oEJLT1qnbdNS:*AE');
define('SECURE_AUTH_SALT', ';DQ>ULCHw7}^tw^mh1Em$B;;BDLI#k0H:piz1x34UWuV`0Ga)vj5Boq-+td#^ikO');
define('LOGGED_IN_SALT',   'Zk$Lx/RffvsO6kZk=9[WXuj73/?J:$&8O=X[i#2QW;<Cb^tzJ/G.`#H~1hN ;]}5');
define('NONCE_SALT',       '8F*g+}B;YZTARJo>yN4/@1El6%`t-<of^Mv;$5i1>X)trYDAjR4r6~Yxw7L;MdDN');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
