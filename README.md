# Docker Base

Defaults:

* MySQL: latest version from docker hub
* PHP - 7.2 with FPM
* Nginx - latest version from docker hub
* wp-cli - latest version from wp-cli.org

You can execute wp-cli commands with the included wp.sh wrapper script. 

```
./wp.sh enter your wp-cli commands here
./wp.sh search-replace 'http://oldurl' 'http://newurl' --dry-run --precise
```

## Running this env

First time running this you will need to build the env. You can do so by executing `docker-compose up -d` 

After this you can start the env with `docker-compose start`

To shut it down, `docker-compose stop`

To destroy it all, `docker-compose down`